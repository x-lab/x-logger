use cpp::{cpp};

cpp! {{
#include <xLogger>
}}

pub fn init() {

    cpp!(unsafe [] {
        x_logger_initialise();
        x_logger_append_console();
    });
}
